#!/bin/bash
set -x

TAG_SHA=$1

if [[ -z "$TAG_SHA" ]] || [[ "$TAG_SHA" = "null" ]]; then
    echo "no TAG_SHA cannot compare commits"
    exit 0
fi

NUM_COMMITS=`git rev-list ${TAG_SHA}..HEAD --count`

if [[ "${NUM_COMMITS}" -le "1" ]]; then
    echo "No changes. No new commits."
    exit 11
fi

exit 0