#!groovy

// prints current directory
void printCurrentDirectory() {
    echo 'current directory'
    sh 'pwd'
}

// prints current build status
void printCurrentBuildResult() {
    echo "current build result: ${currentBuild.currentResult}"
}

// Marks current build as unstable
void markCurrentBuildAsUnstable() {
    currentBuild.result = 'UNSTABLE'
    echo "Current build was marked as unstable"
}

// Marks current build as failed
void markCurrentBuildAsFailed() {
    currentBuild.result = 'FAILURE'
    echo "Current build was marked as failed"
}

return this